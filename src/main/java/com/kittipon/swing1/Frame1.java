/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.swing1;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author kitti
 */
public class Frame1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 300);
        JLabel lblHelloworld = new JLabel("Hello world!!!",JLabel.CENTER);
        frame.add(lblHelloworld);
        lblHelloworld.setBackground(Color.GRAY);
        lblHelloworld.setFont(new Font("Verdana",Font.PLAIN,25));
        lblHelloworld.setOpaque(true);
        
        frame.setVisible(true);        
    }
}
